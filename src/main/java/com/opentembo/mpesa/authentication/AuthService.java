/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.opentembo.mpesa.paybills.PayBill;
import com.opentembo.mpesa.utils.helper.ToolsManager;
import org.apache.commons.codec.binary.Base64;
import com.opentembo.mpesa.utils.options.SystemOptionService;
import javax.annotation.PostConstruct;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author manyala
 */
@Service
public class AuthService {

    @Autowired
    SystemOptionService systemOptionService;

    private Boolean isProduction() {
        return Boolean.valueOf(systemOptionService.getValueByName("IS_PRODUCTION"));
    }

    public AuthResponse getToken(PayBill payBill) {
        AuthResponse response = new AuthResponse();

        if (!isProduction()) {
            response.setError(false);
            response.setMessage(systemOptionService.getValueByName("ACCESS_TOKEN"));
            return response;
        }
        try {
            String app_key = payBill.getConsumerKey();;
            String app_secret = payBill.getConsumerSecret();
            String apiURL = systemOptionService.getValueByName("API_ENDPOINT");
            String appKeySecret = app_key + ":" + app_secret;
            byte[] bytes = appKeySecret.getBytes("ISO-8859-1");
            String auth = new String(Base64.encodeBase64(bytes));

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(apiURL + "/oauth/v1/generate?grant_type=client_credentials")
                    .get()
                    .addHeader("authorization", "Basic " + auth)
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response clientResponse = client.newCall(request).execute();
            if (clientResponse.code() == 200) {
                response.setError(false);

                MpesaAuthResponse mpesaAuthResponse = ToolsManager.getInstance().getJSONObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES).readValue(clientResponse.body().string(), MpesaAuthResponse.class);
                if (mpesaAuthResponse != null) {
                    response.setAccessToken(mpesaAuthResponse.getAccess_token());
                    response.setMessage(clientResponse.message());
                }
            } else {
                response.setMessage(clientResponse.message());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setMessage(ex.getMessage());
        }
        return response;
    }
}
