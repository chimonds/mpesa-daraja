/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.utils.options;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author manyala
 */
@Service
public class SystemOptionServiceImpl implements SystemOptionService {

    @Autowired
    EntityManager entityManager;

    @Autowired
    SystemOptionRepository systemOptionRepository;

    @Override
    public String getValueByName(String name) {
        if (optionExists(name)) {
            return getByName(name).getValue();
        }
        create(name);
        return getByName(name).getValue();
    }

    @Override
    public Boolean optionExists(String name) {
        if(getByName(name)==null){
            return  false;
        }
        return true;
    }

    @Override
    public SystemOption getByName(String name) {
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(QSystemOption.systemOption.name.eq(name));
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(QSystemOption.systemOption).where(builder).singleResult(QSystemOption.systemOption);
    }

    @Override
    public void create(String name) {
        SystemOption option = new SystemOption();
        option.setName(name);
        option.setValue(name);
        option.setDescription(name + " - Auto created");
        systemOptionRepository.save(option);
    }

}
