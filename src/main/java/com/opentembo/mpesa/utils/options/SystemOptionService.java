/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.utils.options;

/**
 *
 * @author manyala
 */
public interface SystemOptionService {

    String getValueByName(String name);
    
    Boolean optionExists(String name);
    
    SystemOption getByName(String name);
    
    void create(String name);
}
