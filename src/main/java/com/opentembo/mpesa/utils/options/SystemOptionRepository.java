/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.utils.options;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author manyala
 */
public interface SystemOptionRepository extends PagingAndSortingRepository<SystemOption, Long>, QueryDslPredicateExecutor<SystemOption>  {
    
}
