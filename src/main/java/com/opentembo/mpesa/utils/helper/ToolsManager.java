/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.utils.helper;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author manyala
 */
public class ToolsManager {

    private static ToolsManager instance = null;
    private ObjectMapper mapper;

    private ToolsManager() {

    }

    public static ToolsManager getInstance() {
        if (instance == null) {
            instance = new ToolsManager();
            instance.mapper = new ObjectMapper();
        }
        return instance;
    }

    public  ObjectMapper getJSONObjectMapper() {
        return mapper;
    }
}
