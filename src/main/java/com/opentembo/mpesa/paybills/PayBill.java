/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

/**
 *
 * @author manyala
 */
@Entity
@Table(name = "pay_bills")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PayBill implements Serializable {

    @Id
    @Column(name = "pay_bill_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long payBillId;

    @Column(name = "short_code", unique = true)
    private String shortCode;

    @Column(name = "company_name")
    private String companyName;
    
    @Column(name = "auth_token")
    private String authToken;
    
    @Column(name = "consumer_key")
    private String consumerKey;
    
     @Column(name = "consumer_secret")
    private String consumerSecret;

    @Column(name = "is_active")
    private Boolean active = true;

    @Column(name = "is_registered")
    private Boolean registered = true;

    @Column(name = "response", length = 1000)
    private String response;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registered_on", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime registeredOn = new DateTime();

    public Long getPayBillId() {
        return payBillId;
    }

    public void setPayBillId(Long payBillId) {
        this.payBillId = payBillId;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getRegistered() {
        return registered;
    }

    public void setRegistered(Boolean registered) {
        this.registered = registered;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public DateTime getRegisteredOn() {
        return registeredOn;
    }

    public void setRegisteredOn(DateTime registeredOn) {
        this.registeredOn = registeredOn;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }  

    public String getConsumerKey() {
        return consumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }
    
    
}