/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills;

import com.opentembo.mpesa.paybills.transactions.client.RemoteClientRequest;
import java.util.List;

/**
 *
 * @author manyala
 */
public interface PayBillService {

    PayBill getById(Long payBillId);

    PayBill getByCode(String code);

    void registerPaybill(Long payBillId);

    List<PayBill> getUnRegisteredPayBills();

    void registerPayBills();

    Boolean clientValid(RemoteClientRequest clientRequest);
}
