/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions;

import org.apache.commons.lang.RandomStringUtils;

/**
 *
 * @author manyala
 */
public class SimulateRequest {

    private String ShortCode = "600522";//601529";
    private String CommandID = "CustomerPayBillOnline";
    private Double Amount = 100.00;
    private String Msisdn = "254708374149";
    private String BillRefNumber = "Q1000";

    public String getShortCode() {
        return ShortCode;
    }

    public void setShortCode(String ShortCode) {
        this.ShortCode = ShortCode;
    }

    public String getCommandID() {
        return CommandID;
    }

    public void setCommandID(String CommandID) {
        this.CommandID = CommandID;
    }

    public Double getAmount() {
        return Amount;
    }

    public void setAmount(Double Amount) {
        this.Amount = Amount;
    }

    public String getMsisdn() {
        return Msisdn;
    }

    public void setMsisdn(String Msisdn) {
        this.Msisdn = Msisdn;
    }

    public String getBillRefNumber() {
        this.BillRefNumber = RandomStringUtils.random(5, true, true).toUpperCase();
        return BillRefNumber;
    }

    public void setBillRefNumber(String BillRefNumber) {
        this.BillRefNumber = BillRefNumber;
    }
}