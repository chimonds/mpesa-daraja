/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions;

import com.opentembo.mpesa.paybills.transactions.client.AllocateRequest;
import com.opentembo.mpesa.paybills.transactions.client.ClientPayBillTransaction;
import com.opentembo.mpesa.paybills.transactions.client.RemoteClientRequest;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author manyala
 */
@RestController
@RequestMapping(value = "/transactions")
public class PayBillTransactionController {

    @Autowired
    PayBillTransactionManager transactionManager;

    @RequestMapping(value = "/validate", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public PayBillTransactionResponse validate(@RequestBody PayBillTransactionRequest billTransactionRequest, HttpServletRequest request, HttpServletResponse response) {
        return transactionManager.validateTransaction(billTransactionRequest);
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public ConfirmationResult confirm(@RequestBody PayBillTransactionRequest billTransactionRequest, HttpServletRequest request, HttpServletResponse response) {
        return transactionManager.confirmTransaction(billTransactionRequest);
    }

    @RequestMapping(value = "/getNotAllocatedTransactions", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public List<ClientPayBillTransaction> getNotAllocatedTransctions(@RequestBody RemoteClientRequest clientRequest, HttpServletRequest request, HttpServletResponse response) {
        return transactionManager.getLast10Transactions(clientRequest);
    }

    @RequestMapping(value = "/allocate", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public Boolean getNotAllocatedTransctions(@RequestBody AllocateRequest allocateRequest, HttpServletRequest request, HttpServletResponse response) {
        return transactionManager.setAllocated(allocateRequest);
    }
}
