/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions.client;

/**
 *
 * @author manyala
 */
public class AllocateRequest extends RemoteClientRequest {

    private Long localTransactionId;

    public Long getLocalTransactionId() {
        return localTransactionId;
    }

    public void setLocalTransactionId(Long localTransactionId) {
        this.localTransactionId = localTransactionId;
    }
}