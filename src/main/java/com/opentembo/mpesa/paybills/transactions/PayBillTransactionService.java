/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions;

import com.opentembo.mpesa.paybills.transactions.client.*;
import java.util.List;

/**
 *
 * @author manyala
 */
public interface PayBillTransactionService {

    void create(PayBillTransactionRequest payBillTransactionRequest);
    
    PayBillTransaction getById(Long transactionId);

    void simulateTransaction();
   

    Boolean exists(PayBillTransactionRequest payBillTransactionRequest);

    List<ClientPayBillTransaction> getLast10Transactions(RemoteClientRequest clientRequest);

    Boolean setAllocated(AllocateRequest request);

    void processFile();
}
