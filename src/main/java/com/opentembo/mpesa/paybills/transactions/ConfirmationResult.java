/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions;

/**
 *
 * @author manyala
 */
public class ConfirmationResult {
   private String C2BPaymentConfirmationResult="Success";

    public String getC2BPaymentConfirmationResult() {
        return C2BPaymentConfirmationResult;
    }

    public void setC2BPaymentConfirmationResult(String C2BPaymentConfirmationResult) {
        this.C2BPaymentConfirmationResult = C2BPaymentConfirmationResult;
    }  
}