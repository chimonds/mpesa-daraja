/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author manyala
 */
public interface PayBillTransactionRepository extends PagingAndSortingRepository<PayBillTransaction, Long>, QueryDslPredicateExecutor<PayBillTransaction>{
    
}
