/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions.client;

import com.opentembo.mpesa.paybills.transactions.PayBillTransactionRequest;

/**
 *
 * @author manyala
 */
public class ClientPayBillTransaction extends PayBillTransactionRequest {

    private Long localTransactionId;

    public Long getLocalTransactionId() {
        return localTransactionId;
    }

    public void setLocalTransactionId(Long localTransactionId) {
        this.localTransactionId = localTransactionId;
    }
}