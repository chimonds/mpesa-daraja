/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions;

import com.opentembo.mpesa.paybills.transactions.client.AllocateRequest;
import com.opentembo.mpesa.paybills.transactions.client.ClientPayBillTransaction;
import com.opentembo.mpesa.paybills.transactions.client.RemoteClientRequest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author manyala
 */
@Service
public class PayBillTransactionManager {

    @Autowired
    PayBillTransactionService payBillTransactionService;

    public PayBillTransactionResponse validateTransaction(PayBillTransactionRequest transactionRequest) {
        return null;
    }

    public ConfirmationResult confirmTransaction(PayBillTransactionRequest transactionRequest) {
        payBillTransactionService.create(transactionRequest);
        return new ConfirmationResult();
    }

    public List<ClientPayBillTransaction> getLast10Transactions(RemoteClientRequest clientRequest) {
        return payBillTransactionService.getLast10Transactions(clientRequest);
    }

    public Boolean setAllocated(AllocateRequest request) {
        return payBillTransactionService.setAllocated(request);
    }

}
