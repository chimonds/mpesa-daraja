/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions;

/**
 *
 * @author manyala
 */
public class PayBillTransactionResponse {
    private Integer ResultCode = 0;
    private String ResultDesc = "Accepted";

    public Integer getResultCode() {
        return ResultCode;
    }

    public void setResultCode(Integer ResultCode) {
        this.ResultCode = ResultCode;
    }

    public String getResultDesc() {
        return ResultDesc;
    }

    public void setResultDesc(String ResultDesc) {
        this.ResultDesc = ResultDesc;
    }   
}