/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions;

import com.opentembo.mpesa.paybills.PayBill;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

/**
 *
 * @author manyala
 */
@Entity
@Table(name = "pay_bill_transactions")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PayBillTransaction implements Serializable {

    @Id
    @Column(name = "pay_bill_transaction_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long payBillTransactionId;

    @Column(name = "transaction_type")
    private String TransactionType;

    @Column(name = "trans_id")
    private String TransID;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trans_time", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime TransTime = new DateTime();

    @Column(name = "trans_amount")
    private Double TransAmount;

    @Column(name = "business_short_code")
    private String BusinessShortCode;

    @Column(name = "bill_ref_number")
    private String BillRefNumber;

    @Column(name = "invoice_number")
    private String InvoiceNumber;

    @Column(name = "org_account_balance")
    private Double OrgAccountBalance = 0.0;

    @Column(name = "third_party_transa_id")
    private String ThirdPartyTransID;

    @Column(name = "msisdn")
    private String MSISDN;

    @Column(name = "firstname")
    private String FirstName;

    @Column(name = "middlename")
    private String MiddleName;

    @Column(name = "lastname")
    private String LastName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime createdOn = new DateTime();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "allocated_on", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime allocatedOn = new DateTime();

    @Column(name = "allocated")
    private Boolean allocated = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pay_bill_id")
    private PayBill payBill;

    public long getPayBillTransactionId() {
        return payBillTransactionId;
    }

    public void setPayBillTransactionId(long payBillTransactionId) {
        this.payBillTransactionId = payBillTransactionId;
    }

    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String TransactionType) {
        this.TransactionType = TransactionType;
    }

    public String getTransID() {
        return TransID;
    }

    public void setTransID(String TransID) {
        this.TransID = TransID;
    }

    public DateTime getTransTime() {
        return TransTime;
    }

    public void setTransTime(DateTime TransTime) {
        this.TransTime = TransTime;
    }

    public Double getTransAmount() {
        return TransAmount;
    }

    public void setTransAmount(Double TransAmount) {
        this.TransAmount = TransAmount;
    }

    public String getBusinessShortCode() {
        return BusinessShortCode;
    }

    public void setBusinessShortCode(String BusinessShortCode) {
        this.BusinessShortCode = BusinessShortCode;
    }

    public String getBillRefNumber() {
        return BillRefNumber;
    }

    public void setBillRefNumber(String BillRefNumber) {
        this.BillRefNumber = BillRefNumber;
    }

    public String getInvoiceNumber() {
        return InvoiceNumber;
    }

    public void setInvoiceNumber(String InvoiceNumber) {
        this.InvoiceNumber = InvoiceNumber;
    }

    public Double getOrgAccountBalance() {
        return OrgAccountBalance;
    }

    public void setOrgAccountBalance(Double OrgAccountBalance) {
        this.OrgAccountBalance = OrgAccountBalance;
    }

    public String getThirdPartyTransID() {
        return ThirdPartyTransID;
    }

    public void setThirdPartyTransID(String ThirdPartyTransID) {
        this.ThirdPartyTransID = ThirdPartyTransID;
    }

    public String getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(String MSISDN) {
        this.MSISDN = MSISDN;
    }

   

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public DateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }

    public DateTime getAllocatedOn() {
        return allocatedOn;
    }

    public void setAllocatedOn(DateTime allocatedOn) {
        this.allocatedOn = allocatedOn;
    }

    public Boolean getAllocated() {
        return allocated;
    }

    public void setAllocated(Boolean allocated) {
        this.allocated = allocated;
    }

    public PayBill getPayBill() {
        return payBill;
    }

    public void setPayBill(PayBill payBill) {
        this.payBill = payBill;
    }
}
