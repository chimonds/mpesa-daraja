/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills.transactions;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.opentembo.mpesa.authentication.AuthService;
import com.opentembo.mpesa.paybills.PayBill;
import com.opentembo.mpesa.paybills.PayBillService;
import com.opentembo.mpesa.paybills.transactions.client.AllocateRequest;
import com.opentembo.mpesa.paybills.transactions.client.ClientPayBillTransaction;
import com.opentembo.mpesa.paybills.transactions.client.RemoteClientRequest;
import com.opentembo.mpesa.utils.helper.ToolsManager;
import com.opentembo.mpesa.utils.options.SystemOptionService;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author manyala
 */
@Service
public class PayBillTransactionServiceImpl implements PayBillTransactionService {

    @Autowired
    SystemOptionService systemOptionService;

    @Autowired
    AuthService authService;

    @Autowired
    PayBillTransactionRepository transactionRepository;

    @Autowired
    PayBillService payBillService;

    @Autowired
    EntityManager entityManager;

    @Override
    public void create(PayBillTransactionRequest payBillTransactionRequest) {
        if (!exists(payBillTransactionRequest)) {
            PayBillTransaction transaction = new PayBillTransaction();
            transaction.setTransID(payBillTransactionRequest.getTransID());

            if (StringUtils.isNotEmpty(payBillTransactionRequest.getBusinessShortCode())) {
                transaction.setBusinessShortCode(payBillTransactionRequest.getBusinessShortCode()); //use to set relationship right
                PayBill payBill = payBillService.getByCode(payBillTransactionRequest.getBusinessShortCode());
                if (payBill != null) {
                    transaction.setPayBill(payBill);
                }
            }

            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
            DateTime transactionTime = formatter.parseDateTime(payBillTransactionRequest.getTransTime());

            transaction.setTransTime(transactionTime); //change to datetime
            transaction.setTransAmount(Double.parseDouble(payBillTransactionRequest.getTransAmount()));//change to double
            transaction.setBillRefNumber(payBillTransactionRequest.getBillRefNumber());
            if (StringUtils.isNotBlank(payBillTransactionRequest.getOrgAccountBalance())) {
                transaction.setOrgAccountBalance(Double.parseDouble(payBillTransactionRequest.getOrgAccountBalance())); //org account balance as double
            }
            if (StringUtils.isNotBlank(payBillTransactionRequest.getThirdPartyTransID())) {
                transaction.setThirdPartyTransID(payBillTransactionRequest.getThirdPartyTransID());
            }

            if (StringUtils.isNotEmpty(payBillTransactionRequest.getMSISDN())) {
                transaction.setMSISDN(payBillTransactionRequest.getMSISDN());
            }

            if (StringUtils.isNotEmpty(payBillTransactionRequest.getFirstName())) {
                transaction.setFirstName(payBillTransactionRequest.getFirstName());
            }

            if (StringUtils.isNotEmpty(payBillTransactionRequest.getMiddleName())) {
                transaction.setMiddleName(payBillTransactionRequest.getMiddleName());
            }

            if (StringUtils.isNotEmpty(payBillTransactionRequest.getLastName())) {
                transaction.setLastName(payBillTransactionRequest.getLastName());
            }
            transactionRepository.save(transaction);
        }
    }

    //@PostConstruct
    @Override
    public void processFile() {
        try {
            Scanner scanner = new Scanner(new File("/Users/manyala/Downloads/nol2.csv"));
            while (scanner.hasNextLine()) {
                String[] record = scanner.nextLine().split(",");
                PayBillTransactionRequest request = new PayBillTransactionRequest();
                request.setTransID(record[0].trim());

                String transTime = record[1].trim().replace(" ", "").replace(":", "").replace("-", "");

                request.setTransTime(transTime);

                request.setTransAmount(record[2].trim());

                request.setOrgAccountBalance(record[3].trim());

                String[] otherParty = record[4].trim().split("-");
                if (otherParty != null) {
                    if (otherParty.length > 0) {
                        try{
                        request.setMSISDN(otherParty[0]);
                        }catch(Exception ex){}
                        try{
                        request.setFirstName(otherParty[1]);
                        }
                        catch(Exception ex){}
                    }
                }

                request.setBillRefNumber(record[5].trim());
                request.setBusinessShortCode("887800");

                sendRequest(request);

            }
            scanner.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println(ex.getMessage());
        }

    }

    OkHttpClient okHttpClient = new OkHttpClient();

    private void sendRequest(PayBillTransactionRequest transactionRequest) {
        try {
            MediaType mediaType = MediaType.parse("application/json");

            String jsonString = ToolsManager.getInstance().getJSONObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.PASCAL_CASE_TO_CAMEL_CASE).writeValueAsString(transactionRequest);

            RequestBody body = RequestBody.create(mediaType, jsonString);

//            https://mpesa.opentembo.io/mpesa/mpesa/confirm
            Request request = new Request.Builder()
                    .url("https://mpesa.opentembo.io/mpesa/mpesa/confirm")
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .build();

            Response response = okHttpClient.newCall(request).execute();
            if (response.code() == 200) {
                System.out.println("Simulation Response:" + response.body().string());
            } else {
                System.out.println("Simulation Response:" + response.body().string());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    //@Scheduled(fixedDelay = 4000)
    public void simulateTransaction() {

        try {

            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            SimulateRequest simulateRequest = new SimulateRequest();
            String jsonString = ToolsManager.getInstance().getJSONObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.PASCAL_CASE_TO_CAMEL_CASE).writeValueAsString(simulateRequest);

            RequestBody body = RequestBody.create(mediaType, jsonString);

            Request request = new Request.Builder()
                    .url(systemOptionService.getValueByName("API_ENDPOINT") + "/mpesa/c2b/v1/simulate")
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .build();

            Response response = client.newCall(request).execute();
            if (response.code() == 200) {
                System.out.println("Simulation Response:" + response.body().string());
            } else {
                System.out.println("Simulation Response:" + response.body().string());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public Boolean exists(PayBillTransactionRequest payBillTransactionRequest) {
        if (payBillTransactionRequest == null) {
            return true;
        }
        if (StringUtils.isEmpty(payBillTransactionRequest.getBusinessShortCode())) {
            return true;
        }

        if (StringUtils.isEmpty(payBillTransactionRequest.getBillRefNumber())) {
            return true;
        }
        return false;
    }

    @Override
    public List<ClientPayBillTransaction> getLast10Transactions(RemoteClientRequest clientRequest) {
        if (!payBillService.clientValid(clientRequest)) {
            return new ArrayList<>();
        }

        PayBill payBill = payBillService.getByCode(clientRequest.getShortCode());
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(QPayBillTransaction.payBillTransaction.payBill.payBillId.eq(payBill.getPayBillId()));
        builder.and(QPayBillTransaction.payBillTransaction.allocated.eq(false));
        JPAQuery query = new JPAQuery(entityManager);
        List<PayBillTransaction> transactions = query.from(QPayBillTransaction.payBillTransaction).where(builder).orderBy(QPayBillTransaction.payBillTransaction.createdOn.desc()).limit(10).list(QPayBillTransaction.payBillTransaction);
        if (transactions == null) {
            return new ArrayList<>();
        }
        List<ClientPayBillTransaction> clientPayBillTransactions = new ArrayList<>();

        for (PayBillTransaction payBillTransaction : transactions) {
            ClientPayBillTransaction clientPayBillTransaction = new ClientPayBillTransaction();
            clientPayBillTransaction.setLocalTransactionId(payBillTransaction.getPayBillTransactionId());
            clientPayBillTransaction.setTransID(payBillTransaction.getTransID());
            clientPayBillTransaction.setTransTime(String.valueOf(payBillTransaction.getTransTime().getMillis()));
            clientPayBillTransaction.setTransAmount(payBillTransaction.getTransAmount().toString());
            clientPayBillTransaction.setBusinessShortCode(payBillTransaction.getBusinessShortCode());
            clientPayBillTransaction.setBillRefNumber(payBillTransaction.getBillRefNumber());
            clientPayBillTransaction.setOrgAccountBalance(payBillTransaction.getOrgAccountBalance().toString());
            clientPayBillTransaction.setThirdPartyTransID(payBillTransaction.getThirdPartyTransID());
            clientPayBillTransaction.setMSISDN(payBillTransaction.getMSISDN());
            clientPayBillTransaction.setFirstName(payBillTransaction.getFirstName());
            clientPayBillTransaction.setMiddleName(payBillTransaction.getMiddleName());
            clientPayBillTransaction.setLastName(payBillTransaction.getLastName());
            clientPayBillTransactions.add(clientPayBillTransaction);

        }
        return clientPayBillTransactions;
    }

    @Override
    public Boolean setAllocated(AllocateRequest clientRequest) {
        if (!payBillService.clientValid(clientRequest)) {
            return false;
        }

        PayBillTransaction transaction = getById(clientRequest.getLocalTransactionId());
        if (transaction == null) {
            return false;
        }

        PayBill payBill = payBillService.getByCode(clientRequest.getShortCode());

        if (payBill.getPayBillId().compareTo(transaction.getPayBill().getPayBillId()) != 0) {
            return false;
        }

        transaction.setAllocated(true);
        transaction.setAllocatedOn(new DateTime());
        transactionRepository.save(transaction);
        return true;
    }

    @Override
    public PayBillTransaction getById(Long transactionId) {
        return transactionRepository.findOne(transactionId);
    }
}
