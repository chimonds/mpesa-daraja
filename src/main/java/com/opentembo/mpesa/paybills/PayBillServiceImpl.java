/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.opentembo.mpesa.authentication.AuthService;
import com.opentembo.mpesa.paybills.transactions.client.RemoteClientRequest;
import com.opentembo.mpesa.utils.helper.ToolsManager;
import com.opentembo.mpesa.utils.options.SystemOptionService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author manyala
 */
@Service
public class PayBillServiceImpl implements PayBillService {

    @Autowired
    EntityManager entityManager;

    @Autowired
    PayBillRepository payBillRepository;

    @Autowired
    SystemOptionService systemOptionService;

    @Autowired
    AuthService authService;

    @Override
    public List<PayBill> getUnRegisteredPayBills() {
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(QPayBill.payBill.active.eq(true));
        builder.and(QPayBill.payBill.registered.eq(false));
        JPAQuery query = new JPAQuery(entityManager);
        List<PayBill> payBills = query.from(QPayBill.payBill).where(builder).list(QPayBill.payBill);
        if (payBills == null) {
            return new ArrayList<>();
        }
        return payBills;
    }

    @Scheduled(fixedDelay = 3000)
    @Transactional
    public void registerPayBills() {
        List<PayBill> payBills = getUnRegisteredPayBills();
        if (!payBills.isEmpty()) {
            for (PayBill payBill : payBills) {
                registerPaybill(payBill.getPayBillId());
            }
        }
    }

    @Override
    public void registerPaybill(Long payBillId) {
        try {
            PayBill payBill = getById(payBillId);
            if (payBill != null) {

                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/json");
                RegisterRequest registerRequest = new RegisterRequest();
                registerRequest.setShortCode(payBill.getShortCode());
                registerRequest.setResponseType("Completed");//Completed,Cancelled
                registerRequest.setValidationURL(systemOptionService.getValueByName("HOST_NEW") + "/transactions/validate");
                registerRequest.setConfirmationURL(systemOptionService.getValueByName("HOST_NEW") + "/transactions/confirm");

                String jsonString = ToolsManager.getInstance().getJSONObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.PASCAL_CASE_TO_CAMEL_CASE).writeValueAsString(registerRequest);

                RequestBody body = RequestBody.create(mediaType, jsonString);

                String authToken = authService.getToken(payBill).getAccessToken();
                Request request = new Request.Builder()
                        .url(systemOptionService.getValueByName("API_ENDPOINT") + "/mpesa/c2b/v1/registerurl")
                        .post(body)
                        .addHeader("authorization", "Bearer " + authToken)
                        .addHeader("content-type", "application/json")
                        .build();

                Response response = client.newCall(request).execute();
                if (response.code() == 200) {
                    payBill.setResponse(response.body().string());
                    payBill.setRegisteredOn(new DateTime());
                    payBill.setRegistered(true);
                    payBill.setAuthToken(authToken);
                    payBillRepository.save(payBill);
                } else {
                    payBill.setResponse(response.body().string());
                    payBill.setRegisteredOn(new DateTime());
                    payBillRepository.save(payBill);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public PayBill getById(Long payBillId) {
        return payBillRepository.findOne(payBillId);
    }

    @Override
    public PayBill getByCode(String code) {
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(QPayBill.payBill.shortCode.eq(code));
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(QPayBill.payBill).where(builder).singleResult(QPayBill.payBill);
    }

    @Override
    public Boolean clientValid(RemoteClientRequest clientRequest) {

        if (StringUtils.isEmpty(clientRequest.getAuthToken()) || StringUtils.isEmpty(clientRequest.getShortCode())) {
            return false;
        }

        BooleanBuilder builder = new BooleanBuilder();
        builder.and(QPayBill.payBill.shortCode.eq(clientRequest.getShortCode()));
        builder.and(QPayBill.payBill.consumerKey.eq(clientRequest.getAuthToken()));

        JPAQuery query = new JPAQuery(entityManager);
        PayBill payBill = query.from(QPayBill.payBill).where(builder).singleResult(QPayBill.payBill);

        if (payBill != null) {
            return true;
        }
        return false;
    }

}
