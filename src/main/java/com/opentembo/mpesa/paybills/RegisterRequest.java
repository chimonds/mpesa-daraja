/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opentembo.mpesa.paybills;

/**
 *
 * @author manyala
 */
public class RegisterRequest {

    private String ShortCode;
    private String ResponseType = "Completed";
    private String ConfirmationURL;
    private String ValidationURL;

    public String getShortCode() {
        return ShortCode;
    }

    public void setShortCode(String ShortCode) {
        this.ShortCode = ShortCode;
    }

    public String getResponseType() {
        return ResponseType;
    }

    public void setResponseType(String ResponseType) {
        this.ResponseType = ResponseType;
    }

    public String getConfirmationURL() {
        return ConfirmationURL;
    }

    public void setConfirmationURL(String ConfirmationURL) {
        this.ConfirmationURL = ConfirmationURL;
    }

    public String getValidationURL() {
        return ValidationURL;
    }

    public void setValidationURL(String ValidationURL) {
        this.ValidationURL = ValidationURL;
    }

}
